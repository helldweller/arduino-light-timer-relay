class Task
{
  private:
    byte _HoursOn;
    byte _HoursOff;

  public:
    Task()
    {
      _HoursOn = 0;
      _HoursOff = 0;
    };
    Task( byte HoursOn, byte HoursOff )
    {
      _HoursOn = HoursOn;
      _HoursOff = HoursOff;
    };
    byte getHoursOn()
    {
      return _HoursOn;
    };
    byte getHoursOff()
    {
      return _HoursOff;
    };
    void setHoursOn(byte Hours)
    {
      _HoursOn = Hours;
    };
    void setHoursOff(byte Hours)
    {
      _HoursOff = Hours;
    };
    bool inHoursRange(byte Hours)
    {
      return _HoursOn > _HoursOff ? ( Hours >= _HoursOn || Hours < _HoursOff ? true : false ) :
                                  ( Hours >= _HoursOn && Hours < _HoursOff ? true : false );
    };
};
