#include <EEPROM.h>

class LightSensor
{
  private:
    int _pin;
    byte _data;
    byte _trigger;
    int _trigger_addr; // 0 - 511
    bool _night;
    byte _threshold;

  public:
    LightSensor()
    {
      _pin = A0;
      _trigger_addr = 0;
      _data = 0;
      _trigger = EEPROM.read(_trigger_addr);
      if ( _trigger < 0 || _trigger > 100 ) {
        _trigger = 50;
      }
      _night = false;
      _threshold = 10;
      pinMode(_pin, INPUT);
    };
    LightSensor(int pin, int trigger_addr, byte trigger_default, byte threshold)
    {
      _pin = pin;
      _trigger_addr = trigger_addr;
      _data = 0;
      _trigger = EEPROM.read(_trigger_addr);
      if ( _trigger < 0 || _trigger > 100 ) {
        _trigger = trigger_default;
      }
      _night = false;
      _threshold = threshold;
      pinMode(_pin, INPUT);
    };
    byte getData() // 0 - 100 %
    { 
      _data = analogRead(_pin)*0.097751711;
      return _data;
    };
    byte getTrigger()
    {
      if ( _trigger < 0 ) {
        _trigger = EEPROM.read(_trigger_addr);
      }
      return _trigger;
    };
    void setTrigger(byte arg)
    { 
      _trigger = arg;
      EEPROM.write(_trigger_addr, arg);
    };
    bool isNight()
    {
      if ( _night == true ) {
        if ( getData() > (getTrigger() + _threshold) ) {
          _night = false;
        }
      } else {
        if ( getTrigger() > (getData() + _threshold) ) {
          _night = true;
        }
      }
      return _night;
    };
};
