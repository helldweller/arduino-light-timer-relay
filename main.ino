#include <Thread.h>
#include <ArduinoJson.h>
#include <LiquidCrystal.h>
#include <LiquidMenu.h>
#include <iarduino_RTC.h>

/*
* Free pins after LCD Keypad Shield 
* Digital out - 13 12 11 3 2 1 0
* Analog  in  - 1 2 3 4 5
*/

// Digital pins
// 0, 1 - Serial rx-tx
byte oneWirePin = 2;
byte lightRelayPin = 3;
byte LcdDB4 = 4;
byte LcdDB5 = 5;
byte LcdDB6 = 6;
byte LcdDB7 = 7;
byte LcdRS  = 8;
byte LcdDAT = 9;
byte LcdBrightnessPin = 10;
byte RTC_RST = 11;
byte RTC_CLK = 12;
byte RTC_DAT = 13;

// Analog pins
int keyPadPin = 0;
int lightSensorPin = 1;
int LcdBrightness = 30;


char* cur_time;
byte light_sensor_data;
byte light_sensor_trigger;
byte HoursOn = 6; // время включения
//byte MinutesOn = 40;
byte HoursOff = 0; // время выключения
//byte MinutesOff = 50;
byte default_menu_timer = 0;
byte default_menu_timeout = 10; //sec

// Chip Enable CE (RST), I/O (DAT),  SCLK (serial clock)
iarduino_RTC time( RTC_DS1302, RTC_RST, RTC_CLK, RTC_DAT );

// lcd
LiquidCrystal lcd(LcdRS,LcdDAT,LcdDB4,LcdDB5,LcdDB6,LcdDB7);

// relay init (int pin, bool default_on)
Relay *lightRelay = new Relay(lightRelayPin, false);

// int pin, int trigger_addr, byte trigger_default, byte threshold
LightSensor *lightSensor = new LightSensor(lightSensorPin, 511, 55, 5);

Task *timer = new Task( HoursOn, HoursOff );

// Thread 1, Logs
Thread LogThread = Thread();

// Thread 2, Light relay control
Thread LightThread = Thread();

// Thread 3, Display render
Thread DisplayThread = Thread();

// Thread 4, Keypad control
Thread KeypadThread = Thread();


LiquidLine screen_1_line_1(0, 0, "Time: ", cur_time);
LiquidLine screen_1_line_2(0, 1, "Light sensor: ", light_sensor_data);
LiquidScreen screen_1(screen_1_line_1, screen_1_line_2);

LiquidLine screen_2_line_1(0, 0, "Trigger: ", light_sensor_trigger);
LiquidLine screen_2_line_2(0, 1, "Time E: ", HoursOn, " D: ", HoursOff);
LiquidScreen screen_2(screen_2_line_1, screen_2_line_2);

//LiquidMenu main_menu(lcd, welcome_screen, screen_1);
LiquidMenu main_menu(lcd);


// Detect keypad keys
byte key()
{
  int val = analogRead(keyPadPin);
  if (val < 50) return 5;         // Rigth
  else if (val < 150) return 3;   // UP
  else if (val < 350) return 4;   // Down
  else if (val < 500) return 2;   // Left
  else if (val < 800) return 1;   // Select
  else if (val <= 1023) return 0;
}

void keypadControl()
{
  switch (key())
  {
    case 1: // Select
      lightSensor->setTrigger(lightSensor->getData());
      break;
    case 2: // Left
      lightSensor->setTrigger( lightSensor->getTrigger() - 1 );
      break;
    case 3: // UP
      main_menu.previous_screen();
      break;
    case 4: // Down
      main_menu.next_screen();
      break;
    case 5: // Rigth
      lightSensor->setTrigger( lightSensor->getTrigger() + 1 );
      break;
  }
}

void writeSerialLog()
{

  // Use arduinojson.org/assistant to compute the capacity.
  StaticJsonBuffer<500> jsonBuffer;

  // Create the root object
  JsonObject& root = jsonBuffer.createObject();
  
  // Add values in the object
  root["datetime"] = time.gettime("d-m-Y, H:i:s, D");
  root["lightSensorData"] = lightSensor->getData();
  root["lightSensorTrigger"] = lightSensor->getTrigger();

  root.printTo(Serial);
  Serial.println();

}

void lightControl()
{
  light_sensor_data = lightSensor->getData();
  time.gettime(); // Update object time
  
  // ( enable/disable relay if time in seted range ) and ( enable relay if night / disable relay if day )
  lightRelay->turn(
    timer->inHoursRange(time.Hours) ? lightSensor->isNight() : false
  );

}

void displayControl()
{
  cur_time = time.gettime("H:i:s");
  light_sensor_data = lightSensor->getData();
  light_sensor_trigger = lightSensor->getTrigger();
  HoursOn = timer->getHoursOn();
  HoursOff = timer->getHoursOff();
  main_menu.update();
  // lcd.clear();
  // lcd.setCursor(0, 0);
  // lcd.print(time.gettime("H:i:s"));
  // lcd.print("  Lux:");
  // lcd.print(lightSensor->getData());
  // // lcd.setCursor(0, 1);
  // // lcd.print("Trgr:");
  // // lcd.print(lightSensor->getTrigger());

  // lcd.setCursor(0, 1);
  // lcd.print("E:");
  // lcd.print(timer->getHoursOn());
  // lcd.print(" D:");
  // lcd.print(timer->getHoursOff());
  // lcd.print(" T:");
  // lcd.print(lightSensor->getTrigger());
}


void setup()
{
  Serial.begin(9600);
  time.begin();
  analogWrite(LcdBrightnessPin, LcdBrightness);
  lcd.begin(16, 2);
  lcd.clear();

  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Hello!");
  delay(3000);
  lcd.clear();

  main_menu.add_screen(screen_1);
  main_menu.add_screen(screen_2);
  //main_menu.update();

  // Thread 1, Logs
  LogThread.onRun(writeSerialLog); // method
  LogThread.setInterval(5000); // ms

  // Thread 2, Light relay control
  LightThread.onRun(lightControl); // method
  LightThread.setInterval(100); // ms

  // Thread 3, Display render
  DisplayThread.onRun(displayControl); // method
  DisplayThread.setInterval(100); // ms

  // Thread 4, Keypad control
  KeypadThread.onRun(keypadControl); // method
  KeypadThread.setInterval(250); // ms


}

void loop()
{

  // Thread 1, Logs
  if (LogThread.shouldRun()) LogThread.run();

  // Thread 2, Light relay control
  if (LightThread.shouldRun()) LightThread.run();

  // Thread 3, Display render
  if (DisplayThread.shouldRun()) DisplayThread.run();

  // Thread 4, Display render
  if (KeypadThread.shouldRun()) KeypadThread.run();

}
