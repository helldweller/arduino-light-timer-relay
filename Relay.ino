class Relay
{
  private:
    int _pin;
    byte _on;

  public:
    Relay()
    {
      _pin = 11;
      pinMode(_pin, OUTPUT);
      _on = false;
      off();
    };
    Relay(int pin, bool default_on)
    {
      _pin = pin;
      pinMode(_pin, OUTPUT);
      _on = default_on;
      _on ? on() : off();
    };
    void on()
    {
      _on = true;
      digitalWrite(_pin, LOW); // Enable relay
    };
    void off()
    {
      _on = false;
      digitalWrite(_pin, HIGH); // Disable relay
    };
    void turn( bool value )
    {
      value ? on() : off();
    }
    bool isOn()
    {
      return _on;
    };
};
